
import {Component} from 'angular2/core';
import {RouteConfig, ROUTER_DIRECTIVES} from 'angular2/router';

import {UserListComponent} from './components/user/user.list.component';
import {UserDetailComponent} from './components/user/user.detail.component';

@Component({
    selector: 'my-app',
    templateUrl: 'app/app.component.html',
    directives: [ROUTER_DIRECTIVES]
})


@RouteConfig([
    { path: '/users', component: UserListComponent, name: 'UserList' },
    { path: '/users/:id', component: UserDetailComponent, name: 'UserDetail' }
])

export class AppComponent {


}


