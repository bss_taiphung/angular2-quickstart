/**
 * Created by nasterblue on 1/29/16.
 */
System.register(['angular2/core', '../../components/user/user.component', 'angular2/common', 'angular2/router'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, user_component_1, common_1, router_1;
    var UserDirective;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (user_component_1_1) {
                user_component_1 = user_component_1_1;
            },
            function (common_1_1) {
                common_1 = common_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            }],
        execute: function() {
            UserDirective = (function () {
                function UserDirective() {
                }
                __decorate([
                    core_1.Input(), 
                    __metadata('design:type', user_component_1.UserComponent)
                ], UserDirective.prototype, "user", void 0);
                UserDirective = __decorate([
                    core_1.Component({
                        selector: 'user-directive',
                        templateUrl: 'app/directives/user/user.directive.html',
                        styles: ["\n    .completed {\n      text-decoration: line-through;\n      color : red;\n    }\n    .incomplete {\n\n    }\n  "],
                        directives: [common_1.NgClass, router_1.ROUTER_DIRECTIVES]
                    }), 
                    __metadata('design:paramtypes', [])
                ], UserDirective);
                return UserDirective;
            })();
            exports_1("UserDirective", UserDirective);
        }
    }
});
//# sourceMappingURL=user.directive.js.map